[Android-FFmpeg-Build](https://bitbucket.org/posinautoss/androidffmpegbuild)
====

* Build environment and scripts for building FFmpeg for the Android platform
* Using the latest build of FFmpeg but can be configured otherwise


Supported Architecture
----
* armv7
* armv7-neon
* x86

Instructions
----
#####Create the build machine
You will need a *nix environment on which to assemble the necessary tools and libraries.
The simplest way to set up an easily reproducible environment is to use [Vagrant](https://www.vagrantup.com/):

At some folder location setup a vagrant box

```
    $mkdir ./ubuntu_trusty_new
    $cd ./ubuntu_trusty_new
    $vagrant init ubuntu/trusty64
```

You now need to configure a shared folder on the host vm that the guest vm can see. This shared folder will allow us to easily grab the FFmpeg binaries on the host which will be built inside the guest vm.

Edit *Vagrantfile* and change the line:

```
    # config.vm.synced_folder "../data", "/vagrant_data"
```

to:

```
    config.vm.synced_folder "./data", "/vagrant_data"
```


Also create the folder *data* at the same level as *Vagrantfile*.


#####Start up the vm

Boot up and login to the guest vm
```
    $vagrant up
    $vagrant ssh
```

#####Install the Android NDK
Download the latest [Android NDK (Linux 64-bit x86)](https://developer.android.com/tools/sdk/ndk/index.html)
```
	$cd ~
	$wget http://dl.google.com/android/ndk/android-ndk-r10d-linux-x86_64.bin
	$chmod a+x android-ndk-r10d-linux-x86_64.bin

	// Need to execute the bin file but it won't work.
	// Solution is documented here: http://askubuntu.com/questions/133389/no-such-file-or-directory-but-the-file-exists
	$file android-ndk-r10d-linux-x86_64.bin
	> android-ndk-r10d-linux-x86_64.bin: ELF 32-bit LSB  executable, Intel 80386, version 1 (SYSV), dynamically linked (uses shared libs), for GNU/Linux 2.6.24, BuildID[sha1]=e66db713307fc18e379de1e2fc79b3f8af074120, stripped

	$sudo dpkg --add-architecture i386
	$sudo apt-get update
	$sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386
	$sudo ./android-ndk-r10d-linux-x86_64.bin
	$rm android-ndk-r10d-linux-x86_64.bin
```

#####Get essential packages for building

```
	$sudo apt-get --quiet --yes install build-essential git autoconf libtool pkg-config gperf gettext yasm
```

#####Get install/build scripts from this repository

```
	$cd /vagrant_data/
	$mkdir ffmpeg-build-android
	$cd ffmpeg-build-android
	$git clone https://posinautinc@bitbucket.org/RioDeJaneiro_Client/androidffmpegbuild.git
```

#####Clean file state, get FFmpeg code
Run the below script to clean out any build artifacts from a previous build and to fetch the FFmpeg source code.

```
	$./init_update_libs.sh
```
Note: Edit the file if you want to fetch a specific commit, tag or branch.

#####Configure

Configure the CPU/platform binaries to build by editing this line in *settings.sh*:
```
	SUPPORTED_ARCHITECTURES=(armeabi-v7a armeabi-v7a-neon x86)
```

Configure how/what components of FFmpeg to compile in *ffmpeg_build.sh*:

```
./configure \
--target-os="$TARGET_OS" \
--cross-prefix="$CROSS_PREFIX" \
--arch="$NDK_ABI" \
--cpu="$CPU" \
--enable-runtime-cpudetect \
--sysroot="$NDK_SYSROOT" \
--pkg-config="${2}/ffmpeg-pkg-config" \
--prefix="${2}/build/${1}" \
--extra-cflags="-I${TOOLCHAIN_PREFIX}/include $CFLAGS" \
--extra-ldflags="-L${TOOLCHAIN_PREFIX}/lib $LDFLAGS" \
--extra-cxxflags="$CXX_FLAGS" \
--disable-shared \
--enable-static \
--disable-everything \
--enable-small \
--disable-debug \
--enable-runtime-cpudetect \
--disable-ffplay \
--disable-network \
--disable-ffserver \
--disable-bsfs \
--disable-doc \
--disable-symver \
--enable-demuxer=image2 \
--disable-version3 \
--disable-protocols \
--enable-protocol=file \
--disable-muxers \
--enable-muxer=mp4 \
--disable-demuxers \
--enable-demuxer=m4v \
--enable-demuxer=mov \
--enable-encoder=aac \
--enable-decoder=aac \
--enable-parser=acc \
--enable-decoder=h264 \
--enable-decoder=rawvideo \
--enable-parser=mpeg4video \
--enable-parser=h264 \
--enable-filter=crop \
--enable-filter=pad \
--enable-filter=scale \
--enable-filters \
--enable-swscale \
--enable-hwaccels || exit 1
```

#####Build FFmpeg

Execute the following script to build FFmpeg for the configured ABIs. Build output will be placed in the *build* folder.

```
	$export ANDROID_NDK=/home/vagrant/android-ndk-r10d
	$./android_build.sh
```


#####Shutdown the vm, copy binaries

Exit the virtual machine

```
    //Inside your virtual machine, type exit at the console to leave
    $exit
```


Halt the virtual machine


```
    $vagrant down
```


The FFmpeg binaries can be copied from *./data/ffmpeg-build-android/build/<platform>/bin*


These will be needed for [Android-FFmpeg-Wrapper](https://bitbucket.org/posinautoss/androidffmpegwrapper)


References
----
1. Build scripts are an adaptation from 
[FFmpeg-Android](http://hiteshsondhi88.github.io/ffmpeg-android/)
