#!/bin/bash

. abi_settings.sh $1 $2 $3

pushd ffmpeg

case $1 in
  armeabi-v7a | armeabi-v7a-neon)
    CPU='cortex-a8'
  ;;
  x86)
    CPU='i686'
  ;;
esac

./configure \
--target-os="$TARGET_OS" \
--cross-prefix="$CROSS_PREFIX" \
--arch="$NDK_ABI" \
--cpu="$CPU" \
--enable-runtime-cpudetect \
--sysroot="$NDK_SYSROOT" \
--pkg-config="${2}/ffmpeg-pkg-config" \
--prefix="${2}/build/${1}" \
--extra-cflags="-I${TOOLCHAIN_PREFIX}/include $CFLAGS" \
--extra-ldflags="-L${TOOLCHAIN_PREFIX}/lib $LDFLAGS" \
--extra-cxxflags="$CXX_FLAGS" \
--disable-shared \
--enable-static \
--disable-everything \
--enable-small \
--disable-debug \
--enable-runtime-cpudetect \
--disable-ffplay \
--disable-network \
--disable-ffserver \
--disable-bsfs \
--disable-doc \
--disable-symver \
--enable-demuxer=image2 \
--disable-version3 \
--disable-protocols \
--enable-protocol=file \
--disable-muxers \
--enable-muxer=mp4 \
--disable-demuxers \
--enable-demuxer=m4v \
--enable-demuxer=mov \
--enable-encoder=aac \
--enable-decoder=aac \
--enable-parser=acc \
--enable-decoder=h264 \
--enable-decoder=rawvideo \
--enable-parser=mpeg4video \
--enable-parser=h264 \
--enable-filter=crop \
--enable-filter=pad \
--enable-filter=scale \
--enable-filters \
--enable-swscale \
--enable-hwaccels || exit 1

make clean
make -j${NUMBER_OF_CORES} && make install || exit 1

popd
