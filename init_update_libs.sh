#!/bin/bash

echo "============================================"
echo "Configuring environment vars..."
echo "============================================"
# Set WORKPATH to current directory (absolute path)
pushd `dirname $0` > /dev/null
WORKPATH=`pwd -P`
popd > /dev/null
echo "Working directory is" $WORKPATH
echo


echo "============================================"
echo "Removing old files (if any)..."
echo "============================================"
rm -rf $WORKPATH/ffmpeg
rm -rf $WORKPATH/build


echo "============================================"
echo "Git clone ffmpeg..."
echo "============================================"
git clone --depth 1 git://source.ffmpeg.org/ffmpeg

